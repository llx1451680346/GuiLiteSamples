cmake_minimum_required(VERSION 2.8)

PROJECT(UIcode)

SET(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR})
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/ffmpeg_include)

FILE(GLOB SOURCE *.cpp)

ADD_LIBRARY(UIcode
    ${SOURCE}
    ${RESOURCE})

#run while make
add_custom_command(TARGET UIcode
    PRE_BUILD
    COMMAND ln -s libavcodec.so.57 ffmpeg_lib/libavcodec.so -f
    COMMAND ln -s libavformat.so.57 ffmpeg_lib/libavformat.so -f
    COMMAND ln -s libavutil.so.55 ffmpeg_lib/libavutil.so -f
    COMMAND ln -s libswscale.so.4 ffmpeg_lib/libswscale.so -f
)

#run after cmake
execute_process(
    COMMAND ${CMAKE_SOURCE_DIR}/.sync_build.sh HelloFFmpeg
)
